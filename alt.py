from dataclasses import dataclass
import sys


# this version of the file is made to run on earlier version then python 3.10


# Make a data class for the toy robot
@dataclass(order=True)
class ToyRobot:
    """Toy Robot class"""
    x: int
    y: int
    facing: str


def is_face_valid(face: str):
    """Check if direction value is valid"""
    if face == "north" or face == "east" or face == "south" or face == "west":
        return True
    else:
        return False


def is_pos_valid(x: int, y: int):
    """Check is position is within board bounds"""
    if -1 < x < 5 and -1 < y < 5:
        return True
    else:
        return False


def toy_turn(direction: str):
    """Rotate toy in a specific direction"""

    global global_toy

    # if toy has not been places then cancel function
    if global_toy.facing == "":
        print("You have not [place]ed the toy!")
        return

    # Could possibly be optimised if I used a number system instead of strings, but it is fast enough for the use case and helps with code readability
    if global_toy.facing == "north":
        if direction == "left":
            global_toy = ToyRobot(global_toy.x, global_toy.y, "west")
        else:
            global_toy = ToyRobot(global_toy.x, global_toy.y, "east")
    elif global_toy.facing == "east":
        if direction == "left":
            global_toy = ToyRobot(global_toy.x, global_toy.y, "north")
        else:
            global_toy = ToyRobot(global_toy.x, global_toy.y, "south")
    elif global_toy.facing == "south":
        if direction == "left":
            global_toy = ToyRobot(global_toy.x, global_toy.y, "east")
        else:
            global_toy = ToyRobot(global_toy.x, global_toy.y, "west")
    elif global_toy.facing == "west":
        if direction == "left":
            global_toy = ToyRobot(global_toy.x, global_toy.y, "south")
        else:
            global_toy = ToyRobot(global_toy.x, global_toy.y, "north")


def toy_move():
    """Move toy forward by one"""

    global global_toy

    # if toy has not been places then cancel function
    if global_toy.facing == "":
        print("You have not [place]ed the toy!")
        return

    # cut down on code by making this its own mini function
    def cant_move():
        print("Unable to move forward, position is off the board!")

    # Add or remove from axis depending on direction
    if global_toy.facing == "north":
        if is_pos_valid(global_toy.x, global_toy.y + 1):
            global_toy = ToyRobot(global_toy.x, global_toy.y + 1, global_toy.facing)
        else:
            cant_move()
    elif global_toy.facing == "east":
        if is_pos_valid(global_toy.x + 1, global_toy.y):
            global_toy = ToyRobot(global_toy.x + 1, global_toy.y, global_toy.facing)
        else:
            cant_move()
    elif global_toy.facing == "south":
        if is_pos_valid(global_toy.x, global_toy.y - 1):
            global_toy = ToyRobot(global_toy.x, global_toy.y - 1, global_toy.facing)
        else:
            cant_move()
    elif global_toy.facing == "west":
        if is_pos_valid(global_toy.x - 1, global_toy.y):
            global_toy = ToyRobot(global_toy.x - 1, global_toy.y, global_toy.facing)
        else:
            cant_move()


def toy_place(x: int, y: int, face: str):
    global global_toy
    """Place toy is specified location and direction"""
    if is_pos_valid(x, y):
        if is_face_valid(face):
            # if toy hasn't been placed yet create one
            if global_toy.facing != "":
                global_toy = ToyRobot(x, y, face)
            else:
                global_toy = ToyRobot(x, y, face)
        else:
            print("Direction is invalid, please try again")
    else:
        print("Position is invalid, please try again")


def main():
    global global_toy

    loop = True

    # Print list of commands on start for the sake of users
    print("Commands: place, move, left, right, report, help, exit")

    # Loop forever so it doesn't just kick you out after the first command
    while loop:
        # turn all input into lower case to account for all variations of capitals inside a word/command
        input_var = input("Enter command: ")

        # Do a check to see if there is a valid command entered
        if input_var is "":
            print("You must enter a command")
            main()

        input_var = input_var.lower()
        input_var_split = input_var.split()

        if input_var_split[0] == "place":
            if len(input_var.split()) > 1:
                toy_place(int(input_var_split[1]), int(input_var_split[2]), input_var_split[3])
            else:
                print("Place requires addition inputs, example:\nplace 1 0 north")
        elif input_var_split[0] == "move":
            toy_move()
        elif input_var_split[0] == "left":
            toy_turn("left")
        elif input_var_split[0] == "right":
            toy_turn("right")
        elif input_var_split[0] == "report":
            print(f"The toy is at {global_toy.x}X {global_toy.y}Y and is facing {global_toy.facing}")
        elif input_var_split[0] == "help":
            print(
                "Commands:\nplace: places toy in specified location\n        Usage: place [x] [y] [direction (north)]\nmove: moves toy one space in the direction it is facing\nleft/right: turns toy in direction specified\nreport: Prints current location of toy\nexit: exit's the game")
        elif input_var_split[0] == "exit":
            loop = False


# Create global emote with an empty direction to detect when the user has not placed the toy yet
global_toy = ToyRobot(0, 0, "")

if __name__ == "__main__":
    main()
