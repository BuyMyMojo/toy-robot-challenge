from dataclasses import dataclass
import sys

# So right off the bat since this is meant to see how I solve issues I will admit I am using PyCharm as my IDE for this.

# First thing's first I want to visualise the grid so I used asciiflow.com to make this comment:
# +-------+-------+-------+-------+-------+
# |       |       |       |       |       |
# |       |       |       |       |       |
# | (0,4) | (1,4) | (2,4) | (3,4) | (4,4) |
# |       |       |       |       |       |
# +-------+-------+-------+-------+-------+
# |       |       |       |       |       |
# |       |       |       |       |       |
# | (0,3) | (1,3) | (2,3) | (3,3) | (4,3) |
# |       |       |       |       |       |
# +-------+-------+-------+-------+-------+
# |       |       |       |       |       |
# |       |       |       |       |       |
# | (0,2) | (1,2) | (2,2) | (3,2) | (4,2) |
# |       |       |       |       |       |
# +-------+-------+-------+-------+-------+
# |       |       |       |       |       |
# |       |       |       |       |       |
# | (0,1) | (1,1) | (2,1) | (3,1) | (4,1) |
# |       |       |       |       |       |
# +-------+-------+-------+-------+-------+
# |       |       |       |       |       |
# |       |       |       |       |       |
# | (0,0) | (1,0) | (2,0) | (3,0) | (4,0) |
# |       |       |       |       |       |
# +-------+-------+-------+-------+-------+


# Make a data class for the toy robot
@dataclass(order=True)
class ToyRobot:
    """Toy Robot class"""
    x: int
    y: int
    facing: str

def is_face_valid(face: str):
    """Check if direction value is valid"""
    if face == "north" or face == "east" or face == "south" or face == "west":
        return True
    else:
        return False

# TODO move board size to variable instead of being hard coded

def is_pos_valid(x: int, y: int):
    """Check is position is within board bounds"""
    if -1 < x < 5 and -1 < y < 5:
        return True
    else:
        return False

def toy_turn(direction: str):
    """Rotate toy in a specific direction"""

    global  global_toy

    # if toy has not been places then cancel function
    if global_toy.facing == "":
        print("You have not [place]ed the toy!")
        return

    # TODO move to a system that shifts through a list instead of doing a match case check for each dirrection

    # TODO modifiy the facing variable directly instead of rwmaking the entire object

    # Could possibly be optimised if I used a number system instead of strings, but it is fast enough for the use case and helps with code readability
    match global_toy.facing:
        case "north":
            match direction:
                case "left":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "west")
                case "right":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "east")
        case "east":
            match direction:
                case "left":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "north")
                case "right":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "south")
        case "south":
            match direction:
                case "left":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "east")
                case "right":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "west")
        case "west":
            match direction:
                case "left":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "south")
                case "right":
                    global_toy = ToyRobot(global_toy.x, global_toy.y, "north")

def toy_move():
    """Move toy forward by one"""

    global global_toy

    # if toy has not been places then cancel function
    if global_toy.facing == "":
        print("You have not [place]ed the toy!")
        return

    # cut down on code by making this its own mini function
    def cant_move():
        print("Unable to move forward, position is off the board!")

    # Add or remove from axis depending on direction
    match global_toy.facing:
        case "north":
            if is_pos_valid(global_toy.x, global_toy.y + 1):
                global_toy = ToyRobot(global_toy.x, global_toy.y + 1, global_toy.facing)
            else:
                cant_move()
        case "east":
            if is_pos_valid(global_toy.x + 1, global_toy.y):
                global_toy = ToyRobot(global_toy.x + 1, global_toy.y, global_toy.facing)
            else:
                cant_move()
        case "south":
            if is_pos_valid(global_toy.x, global_toy.y - 1):
                global_toy = ToyRobot(global_toy.x, global_toy.y - 1, global_toy.facing)
            else:
                cant_move()
        case "west":
            if is_pos_valid(global_toy.x - 1, global_toy.y):
                global_toy = ToyRobot(global_toy.x - 1, global_toy.y, global_toy.facing)
            else:
                cant_move()

# Place the toy where the user specifies
def toy_place(x: int, y: int, face: str):
    global global_toy
    """Place toy is specified location and direction"""
    if is_pos_valid(x, y):
        if is_face_valid(face):
            # if toy hasn't been placed yet create one
            if global_toy.facing != "":
                global_toy = ToyRobot(x, y, face)
            else:
                global_toy = ToyRobot(x, y, face)
        else:
            print("Direction is invalid, please try again")
    else:
        print("Position is invalid, please try again")

def main():
    # Setup global variable for the toy object
    global global_toy

    loop = True

    # Print list of commands on start for the sake of users
    print("Commands: place, move, left, right, report, help, exit")

    # Loop forever so it doesn't just kick you out after the first command
    while loop:
        # turn all input into lower case to account for all variations of capitals inside a word/command
        input_var = input("Enter command: ")

        # Do a check to see if there is a valid command entered
        if input_var is "":
            print("You must enter a command")
            main()

        input_var = input_var.lower()

        input_var_split = input_var.split()

        # TODO eliminate nested if statments

        match input_var_split[0]:
            case "place":
                if len(input_var.split()) > 1:
                    toy_place(int(input_var_split[1]), int(input_var_split[2]), input_var_split[3])
                else:
                    print("Place requires addition inputs, example:\nplace 1 0 north")
            case "move":
                toy_move()
            case "left":
                toy_turn("left")
            case "right":
                toy_turn("right")
            case "report":
                print(f"The toy is at {global_toy.x}X {global_toy.y}Y and is facing {global_toy.facing}")
            case "help":
                print("Commands:\nplace: places toy in specified location\n        Usage: place [x] [y] [direction (north)]\nmove: moves toy one space in the direction it is facing\nleft/right: turns toy in direction specified\nreport: Prints current location of toy\nexit: exit's the game")
            case "exit":
                loop = False

# Create global emote with an empty direction to detect when the user has not placed the toy yet
global_toy = ToyRobot(0, 0, "")

if __name__ == "__main__":
    main()
