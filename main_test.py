import unittest
import main
from tud_test_base import set_keyboard_input, get_display_output


class BasicTestCase(unittest.TestCase):
    def test_basic(self):
        # ┌───────┬───────┬───────┬───────┬───────┐
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # ├───────┼───────┼───────┼───────┼───────┤
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # ├───────┼───────┼───────┼───────┼───────┤
        # │       │       │       │       │       │
        # │       │       │       │       │       │
        # │   ▲   │       │       │       │       │
        # │   │   │       │       │       │       │
        # ├───┼───┼───────┼───────┼───────┼───────┤
        # │   │   │       │       │       │       │
        # │   │   │       │       │       │       │
        # │   │   │       │       │       │       │
        # │   │   │       │       │       │       │
        # ├───┼───┼───────┼───────┼───────┼───────┤
        # │   │   │       │       │       │       │
        # │   │   │       │       │       │       │
        # │  Toy  │       │       │       │       │
        # │       │       │       │       │       │
        # └───────┴───────┴───────┴───────┴───────┘
        set_keyboard_input(["place 0 0 north", "move", "move", "report", "exit"])

        main.main()

        output = get_display_output()

        self.assertEqual(output, ["Commands: place, move, left, right, report, help, exit", "Enter command: ",
                                  "Enter command: ", "Enter command: ", "Enter command: ",
                                  "The toy is at 0X 2Y and is facing north", "Enter command: "])

    # TODO get positions based of the board size variable

    # TODO move to a list based testing method

    def test_pos_valid(self):
        self.assertEqual(main.is_pos_valid(1, 1), True)

    def test_pos_invalid(self):
        self.assertEqual(main.is_pos_valid(17, 1), False)

    def test_face_valid(self):
        self.assertEqual(main.is_face_valid("north"), True)

    def test_face_invalid(self):
        self.assertEqual(main.is_face_valid("nortth"), False)


if __name__ == '__main__':
    unittest.main()
