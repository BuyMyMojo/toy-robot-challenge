
# Toy Robot Challenge

Read [Toy_Robot_Challenge.md](Toy_Robot_Challenge.md) for more info on the actual challenge


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/BuyMyMojo/toy-robot-challenge.git
```

Go to the project directory

```bash
  cd toy-robot-challenge
```

Run `main.py` (Python 3.10+)

```bash
  python3.10 main.py
```

Run `alt.py` (Python 3.9 or lower)

```bash
  python3 main.py
```


## FAQ

#### What versions of python are supported?

`main.py` requires python 3.10 and above while `alt.py` will work with earlier versions of python 3.0

## Lessons Learned

- Match and case is in python 3.10
- I'ts surprisingly easy to move a "toy" along a grid without any visuals
- How to properly use global variables from unisde function in python
## Acknowledgements

 - [The ability to test input() based functions is thanks to Maurício Aniche](https://gist.github.com/mauricioaniche/671fb553a81df9e6b29434b7e6e53491)


## Running Tests

To run tests, run the following command

```bash
  python3.10 main_test.py
```

This file tests:

    - A basic move scenario
    - position validation
    - position invalidation
    - direction validation
    - direction invalidation

